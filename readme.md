Haxo Thumbnail
============

### Installation

Use composer:

```bash
$ composer require haxo/thumbnail
```

Add repository to composer.json
```bash
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://janharsa@bitbucket.org/janharsa/thumbnail.git"
    }
  ]
```



### Usage
```php
{var $image='images/image.jpg'}
<img src="{$image|thumbnail: 150, 150}" />
```

###