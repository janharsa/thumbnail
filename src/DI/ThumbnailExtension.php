<?php

namespace Haxo\Thumbnail\DI;

use Nette\DI\CompilerExtension;

class ThumbnailExtension extends CompilerExtension
{
	public $default = array(
		'wwwDir' => '%wwwDir%',
		'httpRequest' => '@httpRequest',
		'thumbPathMask' => 'images/thumbs/{filename}-{width}x{height}.{extension}',
		'placeholder' => 'http://dummyimage.com/{width}x{height}/efefef/f00&text=Image+not+found',
		'filterName' => 'thumbnail',
	);

	public function loadConfiguration() {
	$config = $this->getConfig($this->default);
	$this->setup($config);
	}

	protected function setup(array $config) {
		$builder = $this->getContainerBuilder();
		$builder->addDefinition($this->prefix('thumbnail'))
			->setClass('Haxo\Thumbnail\Generator')
			->setArguments([
				'config' => $config,
			]);
		if ($builder->hasDefinition('nette.latteFactory')) {
			$definition = $builder->getDefinition('nette.latteFactory');
			$definition->addSetup('addFilter', array($config['filterName'], array($this->prefix('@thumbnail'), 'thumbnail')));
		}
	}

}
